from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoUpdateForm(ModelForm):
    model = TodoList
    fields = "__all__"


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]


class TodoItemUpdateForm:
    class Meta:
        Model = TodoItem
        fields = "__all__"
